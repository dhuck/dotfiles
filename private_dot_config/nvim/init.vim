"--------- CONFIGURATION
set noswapfile						    " no swap file
syntax enable						    " enable syntax
filetype plugin indent on	
set mouse=a						        " enable mouse
set updatetime=30					    " time before updates
set relativenumber					    " line numbers
set history=1000					    " command history
"set signcolumn=yes numberwidth=6	    " signcolumn and available width
set linebreak						    " text wrapping
"set textwidth=100					    " line length
set spell
set smartcase ignorecase incsearch	    " searching and highlighting
set foldmethod=indent foldlevel=2       " code folding
set foldcolumn=2	                    " code folding
set foldlevelstart=99 foldopen+=jump 
set nofoldenable	                    " auto fold open/close
set clipboard=unnamedplus			    " clipboard register
set tabstop=4 softtabstop=0 
set expandtab			                " tabsize and tab
set shiftwidth=4 smarttab
set breakindent						    " smart wrapping indentation
set breakindentopt=shift:4,min:40,sbr
set ruler						        " always show curser
set wrap
set showcmd                             " keystrokes in command line
set splitbelow splitright               " new windows split right/bottom 
set wildmenu wildmode=longest:full,full " command autocompletion
"set timeoutlen=1000 ttimeoutlen=1000    " timeout for keybind presses
set autowrite                           " autosave
set confirm                             " prompt to save not error
set noerrorbells                        " don't fucking ring, dude
set undofile undodir=$HOME/.vim/undo    " persistent undo
set autochdir                           " relative filepaths
if has("nvim")
    set termguicolors                   " better colors
endif

" - - -- --- Plugins ----- -------- -------------
"   install if not
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/plugged')
    " Status bar {
      Plug 'vim-airline/vim-airline'
      Plug 'vim-airline/vim-airline-themes'
    " }
    " File Tree {
      if has("nvim")
          Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': 'python3 -m chadtree deps'}
      endif
    " }
    " Commenting {
      Plug 'preservim/nerdcommenter'
    " }
    " Color Scheme {
      Plug 'catppuccin/nvim', { 'as': 'catppuccin' }
    " }
    " LSP {
      Plug 'github/copilot.vim'
      Plug 'neoclide/coc.nvim', {'branch': 'release'}
      Plug 'pechorin/any-jump.vim'
      if has("nvim")
          Plug 'folke/trouble.nvim'
      endif
    " }
    " Focus Helpers {
      Plug 'junegunn/goyo.vim'
      Plug 'junegunn/limelight.vim'
    " }
    " Lang Plugins {
      Plug 'lervag/vimtex'
      Plug 'xuhdev/vim-latex-live-preview', {'for' :'tex'}
      Plug 'sheerun/vim-polyglot'       " Syntax Highlighting
    " }
    " Git {
      Plug 'tpope/vim-fugitive'
      Plug 'airblade/vim-gitgutter'
    " }
    " Quality of Life {
      Plug 'jiangmiao/auto-pairs'           " auto close delims
      Plug 'tpope/vim-surround'             " edit tags/delimiters in pars
      Plug 'kyazdani42/nvim-web-devicons'   " fancy icons
      Plug 'mhinz/vim-startify'             " start screen
    " }
    " Multiline Editing {
        Plug 'matze/vim-move'                       " move blocks
    " }
call plug#end()

" - - -- --- Plugin Settings
" Coc {
  let g:coc_global_extensions = ['coc-json', 'coc-git', 'coc-python', 'coc-vimtex', 'coc-go', 'coc-rust-analyzer']
" }

" NerdComment {
  let g:NERDSpaceDelims = 1                     " add space after comment char
  let g:NERDCompactSexyComs = 1                 " short syntax in comment blocks
" }
" - - -- --- Shortcuts ---- -------- ------------
" ---- Tab completion
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
" --- CHADtree
" nnoremap <silent> <Leader>nt :CHADopen<CR>

if has("nvim")
lua << EOF
-- this is space for changing the settings of nvim only plugins
require("catppuccin").setup({
    flavour = "mocha", -- latte, frappe, macchiato, mocha
    background = { -- :h background
        light = "latte",
        dark = "mocha",
    },
    transparent_background = true,
    show_end_of_buffer = false, -- show the '~' characters after the end of buffers
    term_colors = false,
    dim_inactive = {
        enabled = false,
        shade = "dark",
        percentage = 0.15,
    },
    no_italic = false, -- Force no italic
    no_bold = false, -- Force no bold
    styles = {
        comments = { "italic" },
        conditionals = { "italic" },
        loops = {},
        functions = {},
        keywords = {},
        strings = {},
        variables = {},
        numbers = {},
        booleans = {},
        properties = {},
        types = {},
        operators = {},
    },
    color_overrides = {},
    custom_highlights = {},
    integrations = {
        cmp = true,
        gitsigns = true,
        nvimtree = true,
        telescope = true,
        notify = false,
        mini = false,
        -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
    },
})

-- setup must be called before loading
vim.cmd.colorscheme "catppuccin"
EOF
endif
